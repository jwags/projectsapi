﻿using AutoMapper;
using Microsoft.Extensions.DependencyInjection;
using ProjectApi.Mapping.Profiles;

namespace ProjectApi.Mapping
{
    public static class AutoMapperProfile
    {
        public static void Build(IServiceCollection services)
        {
            services
                .AddAutoMapper(typeof(ProjectProfile))
                .AddAutoMapper(typeof(ProjectTypeProfile));
        }
    }
}
