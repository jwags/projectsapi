using AutoMapper;
using ProjectApi.Entities;
using ProjectApi.Models;

namespace ProjectApi.Mapping.Profiles
{
    public class ProjectTypeProfile : Profile
    {
        public ProjectTypeProfile()
        {
            this.CreateMap<ProjectTypeProfile, ProjectTypeDto>();
        }
    }
}