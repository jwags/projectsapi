using AutoMapper;
using ProjectApi.Entities;
using ProjectApi.Models;

namespace ProjectApi.Mapping.Profiles
{
    public class ProjectProfile : Profile
    {
        public ProjectProfile()
        {
            this.CreateMap<Project, ProjectDto>();
        }
    }
}