using AutoMapper;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using ProjectApi.Business.Services;
using ProjectApi.ContextConfiguration;
using ProjectApi.Entities;
using ProjectApi.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ProjectApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ProjectsController : ControllerBase
    {
        private readonly IMapper _Mapper;
        private readonly IProjectRepository _ProjectRepository;

        public ProjectsController(ProjectContext context, IProjectRepository projectRepository, IMapper mapper)
        {
            _ProjectRepository = projectRepository;
            _Mapper = mapper;
        }

        // GET: api/Projects/v1
        [HttpGet("v1")]
        public async Task<ActionResult<IEnumerable<Project>>> GetProjects()
        {
            return Ok(await _ProjectRepository.GetProjects());
        }

        // GET: api/Projects/Featured/v1
        [HttpGet("Featured/v1")]
        public async Task<ActionResult<IEnumerable<Project>>> GetFeaturedProjects()
        {
            return Ok(await _ProjectRepository.GetFeaturedProjects());
        }

        // GET: api/Projects/5
        // [HttpGet("v1/{id}")]
        // public async Task<ActionResult<Project>> GetProject(int id)
        // {
        //     var project = await _context.Projects.FindAsync(id);

        //     if (project == null)
        //     {
        //         return NotFound();
        //     }

        //     return project;
        // }

        // PUT: api/Projects/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for
        // more details see https://aka.ms/RazorPagesCRUD.
        // [HttpPut("v1/{id}")]
        // public async Task<IActionResult> PutProject(int id, Project project)
        // {
        //     if (id != project.Id)
        //     {
        //         return BadRequest();
        //     }

        //     _context.Entry(project).State = EntityState.Modified;

        //     try
        //     {
        //         await _context.SaveChangesAsync();
        //     }
        //     catch (DbUpdateConcurrencyException)
        //     {
        //         if (!ProjectExists(id))
        //         {
        //             return NotFound();
        //         }
        //         else
        //         {
        //             throw;
        //         }
        //     }

        //     return NoContent();
        // }

        // POST: api/Projects
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for
        // more details see https://aka.ms/RazorPagesCRUD.
        // [HttpPost("v1")]
        // public async Task<ActionResult<Project>> PostProject(Project project)
        // {
        //     _context.Projects.Add(project);
        //     await _context.SaveChangesAsync();

        //     return CreatedAtAction(nameof(GetProject), new { id = project.Id }, project);
        // }

        // DELETE: api/Projects/5
        // [HttpDelete("v1/{id}")]
        // public async Task<ActionResult<Project>> DeleteProject(int id)
        // {
        //     var project = await _context.Projects.FindAsync(id);
        //     if (project == null)
        //     {
        //         return NotFound();
        //     }

        //     _context.Projects.Remove(project);
        //     await _context.SaveChangesAsync();

        //     return project;
        // }

        // private bool ProjectExists(int id)
        // {
        //     return _context.Projects.Any(e => e.Id == id);
        // }
    }
}
