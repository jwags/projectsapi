using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using ProjectApi.ContextConfiguration;
using ProjectApi.Mapping;
using ProjectApi.Entities;

namespace ProjectApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ProjectTypesController : ControllerBase
    {
        private readonly ProjectTypeContext _context;

        public ProjectTypesController(ProjectTypeContext context)
        {
            _context = context;
        }

        // GET: api/ProjectTypes
        [HttpGet("v1")]
        public async Task<ActionResult<IEnumerable<ProjectType>>> GetProjectTypes()
        {
            return await _context.ProjectTypes.ToListAsync();
        }

        // GET: api/ProjectTypes/5
        [HttpGet("v1/{id}")]
        public async Task<ActionResult<ProjectType>> GetProjectType(int id)
        {
            var projectType = await _context.ProjectTypes.FindAsync(id);

            if (projectType == null)
            {
                return NotFound();
            }

            return projectType;
        }

        // PUT: api/ProjectTypes/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for
        // more details see https://aka.ms/RazorPagesCRUD.
        // [HttpPut("v1/{id}")]
        // public async Task<IActionResult> PutProjectType(int id, ProjectType projectType)
        // {
        //     if (id != projectType.Id)
        //     {
        //         return BadRequest();
        //     }

        //     _context.Entry(projectType).State = EntityState.Modified;

        //     try
        //     {
        //         await _context.SaveChangesAsync();
        //     }
        //     catch (DbUpdateConcurrencyException)
        //     {
        //         if (!ProjectTypeExists(id))
        //         {
        //             return NotFound();
        //         }
        //         else
        //         {
        //             throw;
        //         }
        //     }

        //     return NoContent();
        // }

        // POST: api/ProjectTypes
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for
        // more details see https://aka.ms/RazorPagesCRUD.
        // [HttpPost("v1")]
        // public async Task<ActionResult<ProjectType>> PostProjectType(ProjectType projectType)
        // {
        //     _context.ProjectTypes.Add(projectType);
        //     await _context.SaveChangesAsync();

        //     return CreatedAtAction(nameof(GetProjectType), new { id = projectType.Id }, projectType);
        // }

        // DELETE: api/ProjectTypes/5
        // [HttpDelete("v1/{id}")]
        // public async Task<ActionResult<ProjectType>> DeleteProjectType(int id)
        // {
        //     var projectType = await _context.ProjectTypes.FindAsync(id);
        //     if (projectType == null)
        //     {
        //         return NotFound();
        //     }

        //     _context.ProjectTypes.Remove(projectType);
        //     await _context.SaveChangesAsync();

        //     return projectType;
        // }

        // private bool ProjectTypeExists(int id)
        // {
        //     return _context.ProjectTypes.Any(e => e.Id == id);
        // }
    }
}
