using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using ProjectApi.ContextConfiguration;
using ProjectApi.Mapping;
using ProjectApi.Entities;

namespace ProjectApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class SkillTypesController : ControllerBase
    {
        private readonly SkillTypeContext _context;

        public SkillTypesController(SkillTypeContext context)
        {
            _context = context;
        }

        // GET: api/SkillTypes
        [HttpGet("v1")]
        public async Task<ActionResult<IEnumerable<SkillType>>> GetSkillTypes()
        {
            return await _context.SkillTypes.ToListAsync();
        }

        // GET: api/SkillTypes/5
        [HttpGet("v1/{id}")]
        public async Task<ActionResult<SkillType>> GetSkillType(int id)
        {
            var projectType = await _context.SkillTypes.FindAsync(id);

            if (projectType == null)
            {
                return NotFound();
            }

            return projectType;
        }

        // PUT: api/SkillTypes/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for
        // more details see https://aka.ms/RazorPagesCRUD.
        // [HttpPut("v1/{id}")]
        // public async Task<IActionResult> PutSkillType(int id, SkillType projectType)
        // {
        //     if (id != projectType.Id)
        //     {
        //         return BadRequest();
        //     }

        //     _context.Entry(projectType).State = EntityState.Modified;

        //     try
        //     {
        //         await _context.SaveChangesAsync();
        //     }
        //     catch (DbUpdateConcurrencyException)
        //     {
        //         if (!SkillTypeExists(id))
        //         {
        //             return NotFound();
        //         }
        //         else
        //         {
        //             throw;
        //         }
        //     }

        //     return NoContent();
        // }

        // POST: api/SkillTypes
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for
        // more details see https://aka.ms/RazorPagesCRUD.
        // [HttpPost("v1")]
        // public async Task<ActionResult<SkillType>> PostSkillType(SkillType projectType)
        // {
        //     _context.SkillTypes.Add(projectType);
        //     await _context.SaveChangesAsync();

        //     return CreatedAtAction(nameof(GetSkillType), new { id = projectType.Id }, projectType);
        // }

        // DELETE: api/SkillTypes/5
        // [HttpDelete("v1/{id}")]
        // public async Task<ActionResult<SkillType>> DeleteSkillType(int id)
        // {
        //     var projectType = await _context.SkillTypes.FindAsync(id);
        //     if (projectType == null)
        //     {
        //         return NotFound();
        //     }

        //     _context.SkillTypes.Remove(projectType);
        //     await _context.SaveChangesAsync();

        //     return projectType;
        // }

        // private bool SkillTypeExists(int id)
        // {
        //     return _context.SkillTypes.Any(e => e.Id == id);
        // }
    }
}
