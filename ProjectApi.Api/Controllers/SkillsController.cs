using AutoMapper;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using ProjectApi.Business.Services;
using ProjectApi.ContextConfiguration;
using ProjectApi.Entities;
using ProjectApi.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ProjectApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class SkillsController : ControllerBase
    {
        private readonly IMapper _Mapper;
        private readonly ISkillRepository _SkillRepository;

        public SkillsController(SkillContext context, ISkillRepository projectRepository, IMapper mapper)
        {
            _SkillRepository = projectRepository;
            _Mapper = mapper;
        }

        // GET: api/Skills/v1
        [HttpGet("v1")]
        public async Task<ActionResult<IEnumerable<Skill>>> GetSkills()
        {
            return Ok(await _SkillRepository.GetSkills());
        }

        // GET: api/Skills/SkillType/5/v1
        [HttpGet("SkillType/{id}/v1")]
        public async Task<ActionResult<IEnumerable<Skill>>> GetSkillsBySkillTypeId(int id)
        {
            return Ok(await _SkillRepository.GetSkillsBySkillTypeId(id));
        }

        // GET: api/Skills/v1/5
        // [HttpGet("v1/{id}")]
        // public async Task<ActionResult<Skill>> GetSkill(int id)
        // {
        //     var project = await _context.Skills.FindAsync(id);

        //     if (project == null)
        //     {
        //         return NotFound();
        //     }

        //     return project;
        // }

        // PUT: api/Skills/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for
        // more details see https://aka.ms/RazorPagesCRUD.
        // [HttpPut("v1/{id}")]
        // public async Task<IActionResult> PutSkill(int id, Skill project)
        // {
        //     if (id != project.Id)
        //     {
        //         return BadRequest();
        //     }

        //     _context.Entry(project).State = EntityState.Modified;

        //     try
        //     {
        //         await _context.SaveChangesAsync();
        //     }
        //     catch (DbUpdateConcurrencyException)
        //     {
        //         if (!SkillExists(id))
        //         {
        //             return NotFound();
        //         }
        //         else
        //         {
        //             throw;
        //         }
        //     }

        //     return NoContent();
        // }

        // POST: api/Skills
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for
        // more details see https://aka.ms/RazorPagesCRUD.
        // [HttpPost("v1")]
        // public async Task<ActionResult<Skill>> PostSkill(Skill project)
        // {
        //     _context.Skills.Add(project);
        //     await _context.SaveChangesAsync();

        //     return CreatedAtAction(nameof(GetSkill), new { id = project.Id }, project);
        // }

        // DELETE: api/Skills/5
        // [HttpDelete("v1/{id}")]
        // public async Task<ActionResult<Skill>> DeleteSkill(int id)
        // {
        //     var project = await _context.Skills.FindAsync(id);
        //     if (project == null)
        //     {
        //         return NotFound();
        //     }

        //     _context.Skills.Remove(project);
        //     await _context.SaveChangesAsync();

        //     return project;
        // }

        // private bool SkillExists(int id)
        // {
        //     return _context.Skills.Any(e => e.Id == id);
        // }
    }
}
