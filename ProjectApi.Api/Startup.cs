using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.EntityFrameworkCore;
using ProjectApi.Business.Services;
using ProjectApi.ContextConfiguration;
using ProjectApi.Mapping;
using ProjectApi.Entities;
using Microsoft.AspNetCore.HttpOverrides;

namespace ProjectApi.Api
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddCors(options => options.AddPolicy("ApiCorsPolicy-GetOnly", builder =>
            {
                builder
                    .WithOrigins("http://localhost:4200","https://Dev.JordansPortfolio.com","https://www.Dev.JordansPortfolio.com","https://www.JordansPortfolio.com","https://JordansPortfolio.com")
                    .WithMethods("get")
                    .WithHeaders("application/json");
            }));

            services.AddCors(options => options.AddPolicy("ApiCorsPolicy-AnyCall", builder =>
            {
                builder.WithOrigins("http://localhost:4200")
                    .AllowAnyMethod()
                    .AllowAnyHeader();
            }));

            // register the DbContext on the container
            services.AddDbContext<ProjectContext>(opt =>
               opt.UseInMemoryDatabase("Project"));
            services.AddDbContext<ProjectTypeContext>(opt =>
               opt.UseInMemoryDatabase("ProjectType"));
            services.AddDbContext<SkillContext>(opt =>
               opt.UseInMemoryDatabase("Skill"));
            services.AddDbContext<SkillTypeContext>(opt =>
               opt.UseInMemoryDatabase("SkillType"));

            // register the repository
            services.AddScoped<IProjectRepository, ProjectRepository>();
            services.AddScoped<ISkillRepository, SkillRepository>();

            // register automapper profiles
            AutoMapperProfile.Build(services);

            // register controllers
            services.AddControllers();
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            //app.UseHttpsRedirection();

            app.UseRouting();

            app.UseCors("ApiCorsPolicy-GetOnly");

            // This is not enabled yet. This should only be enabled once a service is put into place to edit, remove, or create projects securley.
            //app.UseCors("ApiCorsPolicy-AnyCall");

            app.UseForwardedHeaders(new ForwardedHeadersOptions
            {
                ForwardedHeaders = ForwardedHeaders.XForwardedFor | ForwardedHeaders.XForwardedProto
            });

            app.UseAuthorization();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
            });
        }
    }
}
