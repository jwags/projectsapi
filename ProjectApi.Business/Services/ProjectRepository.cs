using Microsoft.EntityFrameworkCore;
using ProjectApi.ContextConfiguration;
using ProjectApi.Entities;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace ProjectApi.Business.Services
{
    public interface IProjectRepository
    {
        Task<IEnumerable<Project>> GetProjects();
        Task<IEnumerable<Project>> GetFeaturedProjects();
    }

    public class ProjectRepository : IProjectRepository
    {
        public ProjectRepository(ProjectContext context)
        {
            _Context = context;
        }

        private readonly ProjectContext _Context;

        public async Task<IEnumerable<Project>> GetProjects()
        {
            return await _Context.Projects
                           .Where(project => project.IsVoid == false)
                           .ToListAsync();
        }

        public async Task<IEnumerable<Project>> GetFeaturedProjects()
        {
            return await _Context.Projects
                           .Where(project => project.IsVoid == false && project.IsFeatured == true)
                           .ToListAsync();
        }
    }
}