using Microsoft.EntityFrameworkCore;
using ProjectApi.ContextConfiguration;
using ProjectApi.Entities;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace ProjectApi.Business.Services
{
    public interface ISkillRepository
    {
        Task<IEnumerable<Skill>> GetSkills();
        Task<IEnumerable<Skill>> GetSkillsBySkillTypeId(int skillTypeId);
    }

    public class SkillRepository : ISkillRepository
    {
        public SkillRepository(SkillContext context)
        {
            _Context = context;
        }

        private readonly SkillContext _Context;

        public async Task<IEnumerable<Skill>> GetSkills()
        {
            return await _Context.Skills
                           .Where(skill => skill.IsVoid == false)
                           .ToListAsync();
        }

        public async Task<IEnumerable<Skill>> GetSkillsBySkillTypeId(int skillTypeId)
        {
            return await _Context.Skills
                           .Where(skill => skill.IsVoid == false && skill.SkillTypeId == skillTypeId)
                           .ToListAsync();
        }
    }
}