using Microsoft.EntityFrameworkCore;
using ProjectApi.Entities;

namespace ProjectApi.ContextConfiguration
{
    public class SkillContext : DbContext
    {
        public SkillContext(DbContextOptions<SkillContext> options)
            : base(options)
        {
        }

        public DbSet<Skill> Skills { get; set; }
    }
}