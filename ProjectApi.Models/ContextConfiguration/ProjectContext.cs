using Microsoft.EntityFrameworkCore;
using ProjectApi.Entities;

namespace ProjectApi.ContextConfiguration
{
    public class ProjectContext : DbContext
    {
        public ProjectContext(DbContextOptions<ProjectContext> options)
            : base(options)
        {
        }

        public DbSet<Project> Projects { get; set; }
    }
}