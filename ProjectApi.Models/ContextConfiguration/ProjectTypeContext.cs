using Microsoft.EntityFrameworkCore;
using ProjectApi.Entities;

namespace ProjectApi.ContextConfiguration
{
    public class ProjectTypeContext : DbContext
    {
        public ProjectTypeContext(DbContextOptions<ProjectTypeContext> options)
            : base(options)
        {
        }

        public DbSet<ProjectType> ProjectTypes { get; set; }
    }
}