using Microsoft.EntityFrameworkCore;
using ProjectApi.Entities;

namespace ProjectApi.ContextConfiguration
{
    public class SkillTypeContext : DbContext
    {
        public SkillTypeContext(DbContextOptions<SkillTypeContext> options)
            : base(options)
        {
        }

        public DbSet<SkillType> SkillTypes { get; set; }
    }
}