using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using System;
using ProjectApi.Entities;

namespace ProjectApi.ContextConfiguration
{
    public static class DatabaseGenerator
    {
        public static void Initialize(IServiceProvider serviceProvider)
        {
            using (var context = new ProjectTypeContext(serviceProvider.GetRequiredService<DbContextOptions<ProjectTypeContext>>()))
            {
                context.ProjectTypes.AddRange(
                    new ProjectType
                    {
                        Id = 1,
                        Name = "Website",
                        ThumbnailLocation = "/assets/projectResources/images/websiteThumbnail.svg",
                        IsVoid = false
                    },
                    new ProjectType
                    {
                        Id = 2,
                        Name = "Android App",
                        ThumbnailLocation = "/assets/projectResources/images/phoneAppThumbnail.svg",
                        IsVoid = false
                    },
                    new ProjectType
                    {
                        Id = 3,
                        Name = "Program",
                        ThumbnailLocation = "/assets/projectResources/images/programThumbnail.svg",
                        IsVoid = false
                    });

                context.SaveChanges();
            }

            using (var context = new ProjectContext(serviceProvider.GetRequiredService<DbContextOptions<ProjectContext>>()))
            {
                context.Projects.AddRange(
                    new Project
                    {
                        Id = 1,
                        Name = "ARAMARK E-COMMERCE",
                        Description = "At Aramark, one of the bigger project I support is the McDonalds uniform site. Aramark is the biggest uniform supplier for McDonalds. As part of the contract, we were required to build them a site that employees and managers could log onto and order uniforms. Unfortunately I cannot provide you access to the site, but I can show you its homepage.",
                        ThumbnailLocation = "/assets/projectResources/images/aramarkThumbnail.jpg",
                        ResourceUrl = "https://us-uniforms.mcdonalds.com",
                        ProjectTypeId = 1,
                        IsDownload = false,
                        IsFeatured = true,
                        Order = 3,
                        IsVoid = false
                    },
                    new Project
                    {
                        Id = 2,
                        Name = "BEST OF KINGSTON",
                        Description = "A friend of mine owns two condos in Myrtle Beach, which he rents our all year. I threw together a website for him to advertise his condo, the views, and some of the other great attractions nearby.",
                        ThumbnailLocation = "/assets/projectResources/images/bestOfKingstonThumbnail.jpg",
                        ResourceUrl = "https://bestofkingston.jordansportfolio.com",
                        ProjectTypeId = 1,
                        IsDownload = false,
                        IsFeatured = false,
                        Order = 9,
                        IsVoid = false
                    },
                    new Project
                    {
                        Id = 3,
                        Name = "NORTHERN SAFETY E-COMMERCE",
                        Description = "At Northern Safety, one of the biggest projects I helped support was their E-commerce application. This was a Microsoft Stack application. We used anything from C# and Entity Framework, to Linq and TFS. We also used AngularJS and TypeScript on our front-end. We were very big on keeping our code base clean and well organized, and so we would have code reviews on every change, no matter how small. Please feel free to take a look around.",
                        ThumbnailLocation = "/assets/projectResources/images/northernSafetyThumbnail.jpg",
                        ResourceUrl = "https://northernsafety.com",
                        ProjectTypeId = 1,
                        IsDownload = false,
                        IsFeatured = false,
                        Order = 4,
                        IsVoid = false
                    },
                    new Project
                    {
                        Id = 4,
                        Name = "CRYPTO CALCULATOR",
                        Description = "A while back I invested a little money into Bitcoin through CoinBase. I soon discovered that if I wanted to see how my investment was doing, I would have to go to the website, login in, and confirm my login using a 2-step verification method. This took too long for me. I just wanted to see what the current price was. So I built my first mobile app that called CoineBase's API which returned me the current market price. I let the user enter how much bitcoin (as well as a few other cryptocurrencies) they had, bought, or sold, and it would tell them how much their current investment was worth in USD. It stored their amount on their phone so they would not have to update it everytime.",
                        ThumbnailLocation = "/assets/projectResources/images/cryptoCalculatorThumbnail.jpg",
                        ResourceUrl = "/assets/projectResources/downloads/cryptoCalculator.apk",
                        ProjectTypeId = 2,
                        IsDownload = true,
                        IsFeatured = true,
                        Order = 5,
                        IsVoid = false
                    },
                    new Project
                    {
                        Id = 5,
                        Name = "SAMSUNG REMOTE",
                        Description = "I recently got a new Samsung Smart Tv. I love it, but the only issue is typing. When I want to search a movie or TV show, typing using the remote is horrible. I decided to design a mobile app for the TV that will allow you to pull up a keyboard and type right on your phone. I never got to finish this app, but I was able to connect to the TV and use the standard buttons (volume, channel, menu...). I never got the keyboard to work correctly.",
                        ThumbnailLocation = "/assets/projectResources/images/samsungRemoteThumbnail.jpg",
                        ResourceUrl = "/assets/projectResources/downloads/samsungRemote.apk",
                        ProjectTypeId = 2,
                        IsDownload = true,
                        IsFeatured = false,
                        Order = 7,
                        IsVoid = false
                    },
                    new Project
                    {
                        Id = 6,
                        Name = "SENIOR CAPSTONE PROJECT",
                        Description = "For my senior Capstone project, I developed a web application that would help reduce the struggles for the residential life department during the checkin and checkout proccess of students on the college campuses. This project was developed with the help of BitBucket. More information on the project can be found at our repository. Please contact me for access to the repo and login credentials to the website.",
                        ThumbnailLocation = "/assets/projectResources/images/seniorCapstoneProjectThumbnail.jpg",
                        ResourceUrl = "https://capstone.jordansportfolio.com",
                        ProjectTypeId = 1,
                        IsDownload = false,
                        IsFeatured = true,
                        Order = 6,
                        IsVoid = false
                    },
                    new Project
                    {
                        Id = 7,
                        Name = "GOOD FIBRATIONS",
                        Description = "A friend of mine owned a goat farm. She gathers the wool from her goats and turns it into product that she sells. She dies it, makes yarn out of it, and even knits scarfs and sweaters from the yarn. I made her a webstore where she could log in as an administrator and add, edit, or remove products. She could also change text on the home page. This allowed her to manage her products without needing to involve me.<br />Unfortunately her shop closed down before I had the chance to finish the site. I completed the products page, the product detail page, shopping cart, and even started the checkout process. It took a decent amount of effort and so I left it up on my portfolio.",
                        ThumbnailLocation = "/assets/projectResources/images/goodFibrationsThumbnail.jpg",
                        ResourceUrl = "https://goodfibrations.jordansportfolio.com",
                        ProjectTypeId = 1,
                        IsDownload = false,
                        IsFeatured = false,
                        Order = 1,
                        IsVoid = true
                    },
                    new Project
                    {
                        Id = 8,
                        Name = "POOPSIES PIZZA",
                        Description = "A local pizza bar in my town has great pizza, many customers, but no website. I built them one, but unfortunately they were not interested. I still have it hosted on my server. It is a simple design with almost no functionality, but it is very responsive.",
                        ThumbnailLocation = "/assets/projectResources/images/poopsiesPizzaThumbnail.jpg",
                        ResourceUrl = "https://poopsiespizza.com",
                        ProjectTypeId = 1,
                        IsDownload = false,
                        IsFeatured = false,
                        Order = 1,
                        IsVoid = true
                    },
                    new Project
                    {
                        Id = 9,
                        Name = "BRANCH PREDICTION",
                        Description = "This program is written in python. Its purpose is to simulate three types of branch prediction methods. Fixed-True, Static-First, and Two-Layer Adaptive branch prediction.",
                        ThumbnailLocation = "/assets/projectResources/images/branchPredictionThumbnail.jpg",
                        ResourceUrl = "/assets/projectResources/downloads/CS441-HW4.zip",
                        ProjectTypeId = 3,
                        IsDownload = true,
                        IsFeatured = false,
                        Order = 8,
                        IsVoid = false
                    },
                    new Project
                    {
                        Id = 10,
                        Name = "PACKING WORKBENCH",
                        Description = "This is my main project at my current employer, NOV. It is a web application written in ASP.NET and AngularJS. It is an application to track items for an order or in stock throughout our facility. It is used by various lines of business to tell what parts and equipment go to what orders, how much, and where everything is located. It is also used to track the contents of packages and shipments as well as to generate various reports for compliance, customers, and different departments. Unfortunatley, it is not a project I can share but it was the biggest project I have ever worked on, and I was apart of it from the beginning.",
                        ThumbnailLocation = "/assets/projectResources/images/packingWorkbenchThumbnail.jpg",
                        ResourceUrl = string.Empty,
                        ProjectTypeId = 1,
                        IsDownload = false,
                        IsFeatured = true,
                        Order = 1,
                        IsVoid = false
                    },
                    new Project
                    {
                        Id = 11,
                        Name = "THIS PROJECT",
                        Description = "I just finished rewriting this portfolio. I used .NET Core 3.1 for my Microservices, Angular 8, and Bootstrap 4.4. The code is available publically on my BitBucket account. The <a href='https://bitbucket.org/jwags/jordansportfoliov2/src/master/' target='_blank'>angular repository is here</a>. There are two microservices. One for sending the email on the contact us page, <a href='https://bitbucket.org/jwags/emailapi/src/master/' target='_blank'>located here</a>, and one for pulling the list of my projects, <a href='https://bitbucket.org/jwags/projectsapi/src/master/' target='_blank'>located here</a>. These projects is also hosted on a CentOS 7 VPS I manage myself. This project gave me the ability to explore some technologies I have not really had too much of a chance to work with quite yet.",
                        ThumbnailLocation = "/assets/projectResources/images/jordansPortfolioThumbnail.svg",
                        ResourceUrl = string.Empty,
                        ProjectTypeId = 1,
                        IsDownload = false,
                        IsFeatured = true,
                        Order = 2,
                        IsVoid = false
                    },
                    new Project
                    {
                        Id = 12,
                        Name = "DIVE LOG",
                        Description = "This is my most recent side project. About 2 years ago I became certified to scuba diver. After every dive you are supposed to log various information such as depth, temperature, what weight you used, the equipment you used... I started off using a paper log book but have been wanting to switch to something electronic for a while. I took this opportunity to try and build something myself. I decided to build the backend in .NET Core, which is something I am familiar with. For the frontend, I decided to start with a mobile app. I took this an an opportunity to learn React Native. I have gotten very far with the application, but it is not quite done yet. I have packaged the APK for Android. Feel free to download it and test it. No registration form exists yet, but you can login with Username: Test, Password: Test1234.",
                        ThumbnailLocation = "/assets/projectResources/images/diveSharpThumbnail.jpg",
                        ResourceUrl = "/assets/projectResources/downloads/diveSharpMobile.apk",
                        ProjectTypeId = 2,
                        IsDownload = true,
                        IsFeatured = true,
                        IsVoid = false
                    });

                context.SaveChanges();
            }

            using (var context = new SkillTypeContext(serviceProvider.GetRequiredService<DbContextOptions<SkillTypeContext>>()))
            {
                context.SkillTypes.AddRange(
                    new SkillType
                    {
                        Id = 1,
                        Name = "FRONT-END",
                        IsVoid = false
                    },
                    new SkillType
                    {
                        Id = 2,
                        Name = "BACK-END",
                        IsVoid = false
                    },
                    new SkillType
                    {
                        Id = 3,
                        Name = "TOOLS",
                        IsVoid = false
                    });

                context.SaveChanges();
            }

            using (var context = new SkillContext(serviceProvider.GetRequiredService<DbContextOptions<SkillContext>>()))
            {
                context.Skills.AddRange(
                    new Skill
                    {
                        Id = 1,
                        Name = "Angular 8",
                        ThumbnailLocation = "/assets/projectResources/images/angularThumbnail.svg",
                        SkillTypeId = 1
                    },
                    new Skill
                    {
                        Id = 2,
                        Name = "Bootstrap 4",
                        ThumbnailLocation = "/assets/projectResources/images/bootstrapThumbnail.svg",
                        SkillTypeId = 1
                    },
                    new Skill
                    {
                        Id = 3,
                        Name = "Font Awesome 5",
                        ThumbnailLocation = "/assets/projectResources/images/fontAwesomeThumbnail.svg",
                        SkillTypeId = 1
                    },
                    new Skill
                    {
                        Id = 4,
                        Name = "SASS",
                        ThumbnailLocation = "/assets/projectResources/images/sassThumbnail.svg",
                        SkillTypeId = 1
                    },
                    new Skill
                    {
                        Id = 5,
                        Name = "JQuery",
                        ThumbnailLocation = "/assets/projectResources/images/jqueryThumbnail.svg",
                        SkillTypeId = 1
                    },
                    new Skill
                    {
                        Id = 6,
                        Name = "React",
                        ThumbnailLocation = "/assets/projectResources/images/reactThumbnail.svg",
                        SkillTypeId = 1
                    },
                    new Skill
                    {
                        Id = 7,
                        Name = "AngularJS",
                        ThumbnailLocation = "/assets/projectResources/images/angularJsThumbnail.svg",
                        SkillTypeId = 1
                    },
                    new Skill
                    {
                        Id = 8,
                        Name = "NPM",
                        ThumbnailLocation = "/assets/projectResources/images/npmThumbnail.svg",
                        SkillTypeId = 1
                    },
                    new Skill
                    {
                        Id = 9,
                        Name = ".NET Core 3.1",
                        ThumbnailLocation = "/assets/projectResources/images/dotnetThumbnail.svg",
                        SkillTypeId = 2
                    },
                    new Skill
                    {
                        Id = 10,
                        Name = "Microservices",
                        ThumbnailLocation = "/assets/projectResources/images/microservicesThumbnail.svg",
                        SkillTypeId = 2
                    },
                    new Skill
                    {
                        Id = 11,
                        Name = "Django",
                        ThumbnailLocation = "/assets/projectResources/images/djangoThumbnail.svg",
                        SkillTypeId = 2,
                        IsVoid = true
                    },
                    new Skill
                    {
                        Id = 12,
                        Name = "Entity Framework",
                        ThumbnailLocation = "/assets/projectResources/images/entityFrameworkThumbnail.svg",
                        SkillTypeId = 2
                    },
                    new Skill
                    {
                        Id = 13,
                        Name = "Java",
                        ThumbnailLocation = "/assets/projectResources/images/javaThumbnail.svg",
                        SkillTypeId = 2
                    },
                    new Skill
                    {
                        Id = 14,
                        Name = "Code Igniter",
                        ThumbnailLocation = "/assets/projectResources/images/codeIgniterThumbnail.svg",
                        SkillTypeId = 2
                    },
                    new Skill
                    {
                        Id = 15,
                        Name = "NuGet",
                        ThumbnailLocation = "/assets/projectResources/images/nugetThumbnail.svg",
                        SkillTypeId = 2
                    },
                    new Skill
                    {
                        Id = 16,
                        Name = "SQL",
                        ThumbnailLocation = "/assets/projectResources/images/sqlThumbnail.svg",
                        SkillTypeId = 2
                    },
                    new Skill
                    {
                        Id = 17,
                        Name = "GIT",
                        ThumbnailLocation = "/assets/projectResources/images/gitThumbnail.svg",
                        SkillTypeId = 3
                    },
                    new Skill
                    {
                        Id = 18,
                        Name = "TFS",
                        ThumbnailLocation = "/assets/projectResources/images/tfsThumbnail.svg",
                        SkillTypeId = 3
                    },
                    new Skill
                    {
                        Id = 19,
                        Name = "Agile",
                        ThumbnailLocation = "/assets/projectResources/images/agileThumbnail.svg",
                        SkillTypeId = 3
                    },
                    new Skill
                    {
                        Id = 20,
                        Name = "Scrum",
                        ThumbnailLocation = "/assets/projectResources/images/scrumThumbnail.svg",
                        SkillTypeId = 3
                    },
                    new Skill
                    {
                        Id = 21,
                        Name = "VS Code",
                        ThumbnailLocation = "/assets/projectResources/images/vsCodeThumbnail.svg",
                        SkillTypeId = 3
                    },
                    new Skill
                    {
                        Id = 22,
                        Name = "Visual Studio 2019",
                        ThumbnailLocation = "/assets/projectResources/images/visualStudio2019Thumbnail.svg",
                        SkillTypeId = 3
                    },
                    new Skill
                    {
                        Id = 23,
                        Name = "CI/CD",
                        ThumbnailLocation = "/assets/projectResources/images/cicdThumbnail.svg",
                        SkillTypeId = 3
                    },
                    new Skill
                    {
                        Id = 24,
                        Name = "BitBucket",
                        ThumbnailLocation = "/assets/projectResources/images/bitbucketThumbnail.svg",
                        SkillTypeId = 3
                    },
                    new Skill
                    {
                        Id = 25,
                        Name = "Python",
                        ThumbnailLocation = "/assets/projectResources/images/pythonThumbnail.svg",
                        SkillTypeId = 2
                    });

                context.SaveChanges();
            }
        }
    }
}