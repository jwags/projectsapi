using System.ComponentModel.DataAnnotations;

namespace ProjectApi.Entities
{
    public class ProjectType
    {
        [Key]
        public int Id { get; set; }

        [Required]
        [MaxLength(50)]
        public string Name { get; set; }

        [Required]
        [MaxLength(200)]
        public string ThumbnailLocation { get; set; }

        public bool IsVoid { get; set; }
    }
}