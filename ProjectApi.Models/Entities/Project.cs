using System.ComponentModel.DataAnnotations;

namespace ProjectApi.Entities
{
    public class Project
    {
        [Key]
        public int Id { get; set; }

        [Required]
        [MaxLength(50)]
        public string Name { get; set; }

        [Required]
        [MaxLength(2000)]
        public string Description { get; set; }

        [Required]
        [MaxLength(200)]
        public string ThumbnailLocation { get; set; }
        
        [MaxLength(50)]
        public string ResourceUrl { get; set; }

        [Required]
        public int ProjectTypeId { get; set; }

        public bool IsDownload {get; set; } 

        public bool IsFeatured {get; set; } 

        public int Order {get; set; } 

        public bool IsVoid { get; set; }
    }
}