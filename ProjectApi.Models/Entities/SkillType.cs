using System.ComponentModel.DataAnnotations;

namespace ProjectApi.Entities
{
    public class SkillType
    {
        [Key]
        public int Id { get; set; }

        [Required]
        [MaxLength(50)]
        public string Name { get; set; }

        public bool IsVoid { get; set; }
    }
}