namespace ProjectApi.Models
{
    public class ProjectDto
    {
        public string Name { get; set; }
        public string Description { get; set; }
        public string ThumbnailLocation { get; set; }
        public string ResourceUrl { get; set; }
        public bool IsDownload { get; set; }
        public int Order { get; set; }
    }
}