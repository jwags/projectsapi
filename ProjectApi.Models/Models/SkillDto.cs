namespace ProjectApi.Models
{
    public class SkillDto
    {
        public string Name { get; set; }
        public string ThumbnailLocation { get; set; }
        public int SkillTypeId { get; set; }
    }
}