namespace ProjectApi.Models
{
    public class SkillTypeDto
    {
        public int Id { get; set; }
        public string Name { get; set; }
    }
}