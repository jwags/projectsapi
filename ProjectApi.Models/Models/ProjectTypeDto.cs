namespace ProjectApi.Models
{
    public class ProjectTypeDto
    {
        public string Name { get; set; }
    }
}